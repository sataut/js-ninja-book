// "use strict";

function assert(expression, value){
	
	var container = document.getElementById("container-div");
	 if(!container){
		container = document.createElement("ul");
		container.setAttribute('id', 'container');
		window.document.body.appendChild(container);

	}
	// Create and li with passed value 
	 var li = document.createElement("li");
	 li.className = expression ? "pass": "fail";
	 li.appendChild(document.createTextNode(value));
	 container.appendChild(li);
}

function pass(text){ assert(true, text); }
function fail(text){ assert(false, text); } 
function report(text){ pass(text); }

/** Tests for Assert
*
pass("Should Pass"); 
fail("Should Fail");
report("Should Throw Exception");

*/

/** Original Implementation from Book
*
function assert(value, text) {
  var li = document.createElement("li");
  li.className = value ? "pass" : "fail";
  li.appendChild(document.createTextNode(text));
  var results = document.getElementById("results");
  if (!results) {
    results = document.createElement("ul");
    results.setAttribute('id','results');
    document.body.appendChild(results);
  }
  results.appendChild(li);
}

function pass(text) { assert(true, text); }
function fail(text) { assert(false, text); }
function report(text) { pass(text); }
*/